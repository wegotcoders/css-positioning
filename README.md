# Travel Guide CSS Positioning #

Using CSS to position elements on a webpage.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document, an images folder and CSS style sheets.
2. Add your style rules to style.css, this file is linked to from travel-guide.html.
3. Open travel-guide.html by typing in terminal 'open travel-guide.html', this is a travel guide page with dummy content.
4. Add CSS rules that make the top nav bar sticky (remains at the top of the screen as scrolling up and down page).
5. Add CSS rules that position the hero container in a better place (possibly slightly lower down).
6. Add CSS rules that change the color and position of the destination text (position the text over the photo with contrasting color).
7. Use this page to experiment with positioning and other techniques you have learned, see what you can do.
8. You can see your progress by typing in terminal 'open travel-guide.html'.

(The declarations you wrote in style.css took precedence over the ones found in starter.css because of the order they are loaded in.)
